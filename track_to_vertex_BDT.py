import sys
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import keras
import sklearn
from sklearn.model_selection import train_test_split
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.externals import joblib
from sklearn.datasets import make_classification
from sklearn.ensemble import ExtraTreesClassifier
sys.path.append("%s/../lib" % os.getcwd())
import util
import algos
import tensorflow as tf
from keras import backend as K
import scipy.interpolate
import pickle


def BDT(X, y, track_avg, batch_size, title, is_z0, is_tanL, featureNumber, track_features):

    X_train, X_test, y_train, y_test = sklearn.model_selection.train_test_split(X, y, test_size=0.1, random_state=42)

    bdt = GradientBoostingClassifier(verbose=True)
    bdt.fit(X_train, y_train)
    proba = bdt.predict_proba(X_test)
    loss = sklearn.metrics.log_loss(y_test, proba)

    probPV = bdt.predict_proba(X_test)[:,1]

    effis, puris = [], [] # efficiency, purity
    for cut in np.arange(0., 1., 0.02):
        e, p = util.matchPerf(y_test, y_test[probPV > cut])
        effis.append(e)
        puris.append(p)

    plt.plot(effis, puris, '.')
    plt.xlabel("Efficiency")
    plt.ylabel("Matching Purity")
    plt.title('Efficiency vs Purity ROC for association (' + str(title) + ')')
    plt.savefig('/home/hep/jd918/project/L1_trigger/plots/EffvsPur_'+str(title)+'.png')
    plt.clf()

    AUC = sklearn.metrics.auc(effis, puris)

    file = open('/home/hep/jd918/project/L1_trigger/textfiles/Output'+str(title)+'.txt', "w")
    file.write('AUC of purity vs efficiency: ' + str(AUC))
    file.write("\n")
    auc1 = AUC

    pur = scipy.interpolate.interp1d(effis, puris)
    file.write('Purity at which Efficiency =95%:  ' + str(pur(0.95)))
    pur1 = float(pur(0.95))
    file.write("\n")

    plt.hist(probPV[y_test['fromPV']==1], range=[0., 1.], bins=50, alpha=0.5, density=True)
    plt.hist(probPV[y_test['fromPV']==0], range=[0., 1.], bins=50, alpha=0.5, density=True)
    plt.xlabel("predicted weights")
    plt.title('BDT output (' + str(title) + ')')
    plt.legend(['PV', 'PU'], loc='upper left')
    plt.savefig('/home/hep/jd918/project/L1_trigger/plots/PVvsPU_'+str(title)+'.png')
    plt.clf()

    FPR_BDT, TPR_BDT = [], []
    for cut in np.arange(0., 1., 0.01):
        N = len(y_test) - sum(y_test['fromPV'])
        P = sum(y_test['fromPV'])
        FP = np.sum((probPV > cut) & (y_test['fromPV'] == 0))
        TP = np.sum((probPV > cut) & (y_test['fromPV'] == 1))
        if (N != 0):
            FPR_BDT.append(float(FP)/float(N))
            TPR_BDT.append(float(TP)/float(P))

    AUC = sklearn.metrics.auc(TPR_BDT, FPR_BDT)
    file.write('AUC for traditional ROC curve: ' + str(AUC))
    auc2 = AUC
    file.write("\n")

    fpr = scipy.interpolate.interp1d(FPR_BDT[::-1], TPR_BDT[::-1])
    fpr_BDT1 = float(fpr(0.01))
    file.write('TPR at which FPR = 1%:  ' + str(fpr_BDT1))
    file.write("\n")

    plt.semilogy(TPR_BDT, FPR_BDT)
    plt.hlines(1e-2, 0, fpr_BDT1, linestyle="dashed")
    plt.vlines(fpr_BDT1, 1e-4, 1e-2, linestyle="dashed")
    plt.xlabel("True Positive Rate")
    plt.ylabel("False Positive Rate")
    plt.title('TPR vs FPR ROC for association (' + str(title) + ')')
    plt.savefig('/home/hep/jd918/project/L1_trigger/plots/FPR_vs_TPR_'+str(title)+'.png')
    plt.clf()

    if (str(title[-5:-1])+str(title[-1])) == 'Zmumu':
        title1 = r'$Z \to \mu \mu$'
    else:
        title1 = r'$t \bar{t}$'

    importances = bdt.feature_importances_
    std = np.std(importances, axis=0)
    indices = np.argsort(importances)[::-1]
    file.write(str(indices))
    file.write("\n")

    file.write("Feature ranking:")
    file.write("\n")

    for f in range(X_train.shape[1]):
        file.write("%d. feature %d (%f)" % (f + 1, indices[f], importances[indices[f]]))
        file.write("\n")

    plt.figure()
    plt.title("Feature importances for the BDT (" + str(title1) + ')')
    plt.bar(range(X_train.shape[1]), importances[indices], color="red", align="center")
    plt.xticks(indices[indices], track_features)
    plt.savefig('/home/hep/jd918/project/L1_trigger/plots/feature_importances_'+str(title)+'.png')
    plt.clf()

    file.close()

    dumplings = [auc1, auc2, fpr_BDT1, pur1, loss]
    fileName = '/home/hep/jd918/project/L1_trigger/textfiles/Output'+str(title)+'.pkl'
    fileObject = open(fileName, 'wb')
    pickle.dump(dumplings, fileObject)
    pickle.dump(TPR_BDT, fileObject)
    pickle.dump(FPR_BDT, fileObject)
    pickle.dump(effis, fileObject)
    pickle.dump(puris, fileObject)
    file.close()
