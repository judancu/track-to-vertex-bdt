import track_to_vertex_BDT
import sys
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import util
import algos


events = util.loadData(filename='/home/hep/jd918/project/L1_trigger/data/tmtt-kf4param-ntuple-ttbar-pu200-fall17d.root')
#events = util.loadData(filename='/home/hep/jd918/project/L1_trigger/data/tmtt-kf4param-ntuple-ttbar-pu200-938relval.root')

track_features = ['pt', 'z0', 'eta', 'chi2', 'tanL', 'dz']
truth_label = ['fromPV']
featureNumber = len(track_features)

alltracks = pd.concat(events, ignore_index=True)

track_avg = float(len(alltracks))/float(len(events))
track_avg = round(track_avg)

X = alltracks[track_features]
y = alltracks[truth_label]

batch_size = 400
is_z0 = True
is_tanL = True

title = 'BDT_basic'

track_to_vertex_BDT.BDT(X, y, track_avg, batch_size, title, is_z0, is_tanL, featureNumber)
