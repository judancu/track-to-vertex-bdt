import statsmodels.api as sm
import numpy as np
from sklearn.cluster import DBSCAN

def kde(event, bw=0.2, weight_field='pt', kernel='uni'):
  kde = sm.nonparametric.KDEUnivariate(event['z0'].astype('double'))
  if weight_field is not None:
    kde.fit(kernel=kernel, weights=event[weight_field], fft=False, bw=bw, gridsize=300, clip=(-15., 15.))
  else:
    kde.fit(kernel=kernel, fft=False, bw=bw)
  return kde.support, kde.density

def kdeVX(event, d=None, bw=0.2, weight_field='pt', kernel='uni'):
  # d can be provided if already computed
  if d is None:
    d = kde(event, bw=bw, weight_field=weight_field, kernel=kernel)
  # Can have more than one point with max value
  # find the mean
  iMax = np.where(d[1] == d[1].max())[0].mean()
  if iMax == np.floor(iMax):
    return d[0][int(iMax)]
  else:
    return (d[0][int(np.floor(iMax))] + d[0][int(np.ceil(iMax))]) / 2

def kdeVXTrks(event, d=None, bw=0.2, weight_field='pt', kernel='uni', density_threshold=0.):
  if d is None:
    d = kde(event, bw=bw, weight_field=weight_field, kernel=kernel)
  iMax = np.where(d[1] == d[1].max())[0].mean()
  zeros = np.where(d[1] <= density_threshold)[0]
  # Get the first zero above and below the maximum
  iAbove = min(zeros[zeros > iMax])
  iBelow = max(zeros[zeros < iMax])
  zMax = d[0][iAbove]
  zMax = min([zMax, d[0][int(iMax)] + 0.75]) # Limit the window in which to include all tracks
  zMin = d[0][iBelow]
  zMin = max([zMin, d[0][int(iMax)] - 0.75]) # Limit the window in which to include all tracks
  return event[event['z0'].between(zMin, zMax, inclusive=False)]

def fastHisto(event, weight_field='pt', bins=np.arange(-15., 15., 0.1)):
  weights = None if weight_field is None else event[weight_field]
  h, b = np.histogram(event['z0'], bins=bins, weights=weights)
  c = b + (b[1] - b[0]) / 2 # bin centres
  return h, c

def fastHistoVX(event, d=None, weight_field='pt', bins=np.arange(-15., 15., 0.1)):
  if d is None:
    d = fastHisto(event, weight_field, bins)
  h, c = d
  iMax = h.argmax() # bin with the maximum weight
  # return the weighted sum of 3 bins about the maximum
  limH = iMax + 2 if iMax < len(h) - 1 else iMax + 1
  limL = iMax - 1 if iMax > 0 else 0
  if sum(h[limL:limH]) > 0:
    return sum(c[limL:limH] * h[limL:limH]) / sum(h[limL:limH])
  else:
    print("Sum of weights = 0")
    return 0

def TPMatching(event, reco_z0, res=0.33):
  # 'Adaptive matching' for barrel/endcap tracks
  res = np.array([1.5 * res if eta < 1.2 else 4 * res for eta in event['eta']])
  delta = abs(event['z0'] - reco_z0)
  return event[delta < res]

def fastHistoMatchingPerTracks(tracks, adaptive=False, res=0.33):
  """ Track-to-vertex matching for FastHisto based on tracks

  Matching is performed on individual track objects by looking at their
  separation from the reconstructed vertex (via dz field). One can enable
  adaptive matching by setting a different resolution for forward tracks.

  Keyword arguments:
  tracks   -- dataframe with tracks
  adaptive -- boolean flag that configures whether to cut on dz depending on track position (barrel/endcap)
  res      -- resolution which suggests characteristic separation between a track and a reco vertex
  """
  if adaptive:
    thresh = np.array([1.5 * res if eta < 1.2 else 4 * res for eta in tracks['eta']])
  else:
    thresh = res
  # return tracks[abs(tracks['dz']) < thresh]
  return abs(tracks['dz']) < thresh

def dbscan(event, eps=0.2, min_samples=2, weight_field='pt'):
  """
  Perform DBSCAN clustering on the tracks in event.
  Returns the tracks associated to the highest pt cluster.
  """
  X = np.array(event['z0']).reshape(-1, 1)
  if not weight_field is None:
    w = np.array(event[weight_field]).reshape(-1, 1)
  if weight_field is None:
    clusters = DBSCAN(eps=eps, min_samples=min_samples).fit(X)
  else:
    clusters = DBSCAN(eps=eps, min_samples=min_samples).fit(X, w)
  nCl = max(clusters.labels_) # Number of clusters
  if nCl > 0:
    # Sum the pt for each cluster
    cluster_pts = np.zeros(shape=(nCl))
    for i in range(nCl):
      cluster_pts[i] = sum(event['pt'][clusters.labels_ == i])
    iCl = cluster_pts.argmax() # Label of the max pt vertex
    # Return only the tracks with the label of the highest pt cluster
    return event[clusters.labels_ == iCl]
  else:
    return event[np.zeros(shape=len(event)).astype('bool')]
